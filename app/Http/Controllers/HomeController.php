<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $authUserId = Auth::user()->id;

        $getProfileUser = User::where('id', Auth::user()->id)->first()->toArray();

//        if ( empty($getProfileUser['name'])  or empty($getProfileUser['sex']) or empty($getProfileUser['test']) ){
//
//            return redirect()
//                ->back()
//                ->with('success', 'Your message has been sent successfully!');
//        }else{
//            $getAllUsers = User::get()->toArray();
//        }

        $getAllUsers = User::get()->toArray();


        return view('profile', [
            'getProfileUser' => $getProfileUser,
            'getAllUsers' => $getAllUsers
        ]);
    }

    public function update(Request $request){
        $user = User::find($request['user']);

        DB::table('users')
            ->where('id', $user['id'])
            ->update([
                'name' => $request['name'],
                'description' => $request['description'],
                'sex' => $request['sex']
            ]);
        return redirect()->back();
    }


}
