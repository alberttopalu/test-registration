<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => 'auth'], function () {

    Route::get('/profile', 'HomeController@index')->name('profile');
    Route::post('/profile/{user}', 'HomeController@update')->name('profile-update');
    Route::get('/profile/show', 'HomeController@show')->name('profile-show');

});
