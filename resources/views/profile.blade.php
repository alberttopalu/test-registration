@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                            <table class="table">
                                <h2>Auth User</h2>
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>description</th>
                                    <th>sex</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$getProfileUser['id']}}</td>
                                    <td>{{$getProfileUser['name']}}</td>
                                    <td>{{$getProfileUser['description']}}</td>
                                    <td>{{$getProfileUser['sex']}}</td>


                                    <td class="col-md-2" data-table-header="Actions">
                                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal"
                                                data-target="#EditModal{{$getProfileUser['id']}}">Edit
                                        </button>


                                    </td>
                                </tr>


                                </tbody>
                            </table>
                        </div>

                        {{---------------------modal update---------------}}

                        <div class="modal" id="EditModal{{$getProfileUser['id']}}" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Information</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ url('/profile',  \Illuminate\Support\Facades\Auth::user()->id )}}"
                                              method="POST" enctype="multipart/form-data"
                                              name="modal.{{$getProfileUser['id']}}.">
                                            {{ csrf_field() }}


                                            <div class="form-group row">
                                                <label for="name" class="col-sm-3 col-form-label">Name</label>
                                                <div class="col-sm-9">
                                                    <input name="name" type="text" value="{{$getProfileUser['name']}}"
                                                           class="form-control">
                                                    <span class="invalid-feedback">
                                                    <strong></strong>
                                                    </span>
                                                </div>
                                            </div>





                                                <div class="form-group row">
                                                    <label for="description" class="col-sm-3 col-form-label">Description
                                                    </label>
                                                    <div class="col-sm-9">
                                                        <input name="description"
                                                               value="{{$getProfileUser['description']}}"
                                                               type="text" class="form-control">
                                                        <span class="invalid-feedback">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>


                                            <div class="form-group row">
                                                <label for="sex" class="col-sm-3 col-form-label">Sex</label>
                                                <div class="col-sm-9">
                                                    <input name="sex" value="{{$getProfileUser['sex']}}" type="text"
                                                           class="form-control">
                                                    <span class="invalid-feedback">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>


                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Close
                                                </button>
                                                <button type="submit" class="btn btn-primary" id="submit">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-----------------END modal update---------------}}


                        <div class="table-responsive">
                            <table class="table">
                                <h2>List users</h2>
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($getAllUsers as $user)
                                    <tr>
                                        <td>{{$user['id']}}</td>
                                        <td>{{$user['name']}}</td>

                                        <td class="col-md-2" data-table-header="Actions">
                                            @if(  (empty(\Illuminate\Support\Facades\Auth::user()->name) or  (empty(\Illuminate\Support\Facades\Auth::user()->sex) )  or (empty(\Illuminate\Support\Facades\Auth::user()->description)) )  )
                                                <p class="alert alert-danger">Доступ имеет только пользователь с
                                                    заполненными полями Имя, Пол и О Себе </p>

                                                <button type="button" disabled class="btn btn-info btn-lg"
                                                        data-toggle="modal"
                                                        data-target="#showModal{{$user['id']}}">Show profile
                                                </button>
                                            @else

                                                <button type="button" class="btn btn-info btn-lg"
                                                        data-toggle="modal"
                                                        data-target="#showModal{{$user['id']}}">Show profile
                                                </button>

                                            @endif
                                        </td>
                                    </tr>

                                    {{---------------------modal show---------------}}

                                    <div class="modal" id="showModal{{$user['id']}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Show Information</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-3 col-form-label">Name</label>
                                                        <div class="col-sm-9">
                                                            <input name="name" type="text" disabled="disabled"
                                                                   value="{{$user['name']}}"
                                                                   class="form-control">
                                                            <span class="invalid-feedback">
                                                    <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="description" class="col-sm-3 col-form-label">Description</label>
                                                        <div class="col-sm-9">
                                                            <input name="description" disabled="disabled"
                                                                   value="{{$user['description']}}"
                                                                   type="text" class="form-control">
                                                            <span class="invalid-feedback">
                                                        <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <label for="sex" class="col-sm-3 col-form-label">Sex</label>
                                                        <div class="col-sm-9">
                                                            <input name="sex" disabled="disabled"
                                                                   value="{{$user['sex']}}" type="text"
                                                                   class="form-control">
                                                            <span class="invalid-feedback">
                                                        <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>


                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-----------------END modal show---------------}}
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
